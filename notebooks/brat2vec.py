# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.5.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
#
# Make page wider.

# %%
from IPython.core.display import display, HTML

display(HTML("<style>.container { width:95%; }</style>"))

# %% [markdown]
# Enable nicer plots.

# %%
# %matplotlib notebook

# %% [markdown] pycharm={"name": "#%% md\n"}
# # brat2vec: Word embeddings visualizer for BRAT annotated files

# %% [markdown] pycharm={"name": "#%% md\n"}
# This is a prototype using `gensim`.
#
# Let's first indicate our source data.

# %%
SOURCE = "../data/"

# %% [markdown] pycharm={"name": "#%% md\n"}
# Declare an iterator over all sentences from all input files.

# %% pycharm={"name": "#%%\n"}
from gensim.utils import tokenize


class DocumentIterator(object):

    def __iter__(self):
        path = SOURCE
        files = os.listdir(path)

        for filename in files:
            if not filename.endswith(".txt"):
                continue
            with open(path + "/" + filename, "r", encoding="utf-8") as file:
                text = ' '.join(file.readlines())
                yield list(tokenize(text))


# %% [markdown] pycharm={"name": "#%% md\n"}
# Let's train a FastText model.

# %% pycharm={"name": "#%%\n"}
from gensim.models import FastText

model = FastText(size=100, window=5, min_count=1)
model.build_vocab(sentences=DocumentIterator())
total_examples = model.corpus_count
print(total_examples)
model.train(sentences=DocumentIterator(), total_examples=total_examples, epochs=5)

# %% [markdown] pycharm={"name": "#%% md\n"}
#
# ## Entities
#
# Which types should we color?
#

# %% pycharm={"name": "#%%\n"}
TYPES = {"Organization", "Country"}

# %% [markdown] pycharm={"name": "#%% md\n"}
# Declare an iterator over all entities from all `.ann` files.

# %% pycharm={"name": "#%%\n"}
import os


class AnnotationIterator(object):
    TYPE_FIELD = 1
    TEXT_FIELD = 2

    def __iter__(self):
        path = SOURCE
        files = os.listdir(path)

        for filename in files:
            if not filename.endswith(".ann"):
                continue
            with open(path + "/" + filename, "r", encoding="utf-8") as file:
                for line in file:
                    fields = line.strip().split("\t")
                    entity_type = fields[self.TYPE_FIELD].split(" ")[0]
                    if entity_type in TYPES:
                        yield fields[self.TEXT_FIELD]


# %% [markdown]
#
# We can also import entities from a dictionary.
#
# Here, we import entities from the ASP register of drug names
# (https://aspregister.basg.gv.at/aspregister/faces/aspregister.jspx?_afrLoop=2563780283761485&_afrWindowMode=0&_adf.ctrl-state=qqixkp980_4).

# %%
import pandas as pd

DICTIONARY = "../ASP-Register.xlsx"
USAGES = {"Human"}


class DictionaryIterator(object):
    NAME_FIELD = 'Name'
    USAGE_FIELD = 'Verwendung'

    def __iter__(self):
        path = DICTIONARY

        # TODO stream file (e.g. using `nrows`)
        df = pd.read_excel(DICTIONARY, usecols=[self.NAME_FIELD, self.USAGE_FIELD])

        for row in df.itertuples():
            entity_type = getattr(row, self.USAGE_FIELD)
            if entity_type in USAGES:
                # First token
                yield getattr(row, self.NAME_FIELD).split(" ")[0]


# %% [markdown]
#
# ## Plotting
#
# Here we declare a `display_words` function to ease plotting.

# %% pycharm={"name": "#%%\n"}
from matplotlib import pyplot

import mplcursors


def display_words(data, words, colors, filename):
    fig, ax = pyplot.subplots()
    pyplot.scatter(data[:, 0], data[:, 1], c=colors, s=5)

    # Display word on mouse over
    mplcursors.cursor(ax).connect(
        "add", lambda sel: sel.annotation.set_text(words[sel.target.index]))
    pyplot.show()

    fig.savefig(filename, bbox_inches="tight")


# %% [markdown] pycharm={"name": "#%% md\n"}
# Prepare data.

# %% pycharm={"name": "#%%\n"}
from sklearn.decomposition import PCA

import random

random.seed(42)

COUNT = 10


def add_entities(iterator):
    entities = set()
    for entity in iterator:
        words.add(entity)
        entities.add(entity)
        if len(entities) >= COUNT:
            break
    return entities


# Randomly select COUNT words.
start = random.randint(0, len(model.wv.vocab) - COUNT)
corpus = set(list(model.wv.vocab)[start: start + COUNT])
words = corpus.copy()

# Select the first COUNT entities.
annotation_iter = AnnotationIterator()
dictionary_iter = DictionaryIterator()

annotations = add_entities(annotation_iter)
register = add_entities(dictionary_iter)

assert len(words) > len(corpus)

# We need a list (not a set) to plot.
words = list(words)

# Default color is black.
colors = ['black'] * len(words)

# But entities are colored differently. Colors mix accordingly (e.g. green = blue + yellow).
i = 0
for word in words:
    is_corpus = False
    is_annotated = False
    is_registered = False
    if word in corpus:
        is_corpus = True
        colors[i] = "blue"
    if word in annotations:
        is_annotated = True
        colors[i] = "red"
    if word in register:
        is_registered = True
        colors[i] = "gold"
    if is_annotated and is_registered:  # annotated ==> corpus
        colors[i] = "orange"
    if is_registered and is_corpus and not is_annotated:
        colors[i] = "green"
    i += 1


# %% [md]
#
# Here we do PCA.

# %%
X = model[words]
pca = PCA(n_components=2)
data = pca.fit_transform(X)

display_words(data, words, colors, "pca.png")

# %% [md]
#
# And here t-SNE.

# %%
from sklearn.manifold import TSNE

X = model[words]
tsne = TSNE(n_components=2, random_state=0)
data = tsne.fit_transform(X)

display_words(data, words, colors, "tsne.png")
