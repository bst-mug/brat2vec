# brat2vec

Word embeddings visualizer for BRAT annotated files.

This project aims to solve the question "Do drug name embeddings form distinct clusters and, if yes, can we identify them?"